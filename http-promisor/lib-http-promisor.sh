# Shell functions to upload files on a HTTP server.

SERVER_REPO="gitlab-git.git"

upload_blob() {
	HASH="$1"

	test -n "$HASH" || die "Invalid argument '$HASH'"
	HASH_SIZE=$(git -C "$SERVER_REPO" cat-file -s "$HASH") || {
		echo >&2 "Cannot get blob size of '$HASH'"
		return 1
	}

	UPLOAD_URL="http://127.0.0.1:10000/upload/?sha1=$HASH&size=$HASH_SIZE&type=blob"

	git -C "$SERVER_REPO" cat-file blob "$HASH" >object &&
	curl --data-binary @object --include "$UPLOAD_URL"
}

upload_blobs_from_stdin() {
	while read -r blob
	do
		echo "uploading $blob"
		upload_blob "$blob" || return
	done
}
