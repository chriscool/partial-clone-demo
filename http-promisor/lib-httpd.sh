# Shell library to run an HTTP server.
#
# Based on Git's t/lib-httpd.sh library.
#
# Usage:
#
#	TEST_DIRECTORY=/path/to/this/lib
#	. "$TEST_DIRECTORY"/lib-httpd.sh
#	start_httpd
#
#	...
#
#	stop_httpd
#
# Can be configured using the following variables.
#
#    LIB_HTTPD_PATH              web server path
#    LIB_HTTPD_MODULE_PATH       web server modules path
#    LIB_HTTPD_PORT              listening port
#    LIB_HTTPD_SSL               enable SSL
#
# Copyright (c) 2008 Clemens Buchacher <drizzd@aon.at>
# Copyright (c) 2020 Christian Couder <christian.couder@gmail.com>

test -z "$TEST_DIRECTORY" && TEST_DIRECTORY="$PWD"
echo "TEST_DIRECTORY: '$TEST_DIRECTORY'"

SHELL_PATH="/bin/sh"

HTTPD_PARA=""

for DEFAULT_HTTPD_PATH in '/usr/sbin/httpd' '/usr/sbin/apache2'
do
	if test -x "$DEFAULT_HTTPD_PATH"
	then
		break
	fi
done

for DEFAULT_HTTPD_MODULE_PATH in '/usr/libexec/apache2' \
				 '/usr/lib/apache2/modules' \
				 '/usr/lib64/httpd/modules' \
				 '/usr/lib/httpd/modules'
do
	if test -d "$DEFAULT_HTTPD_MODULE_PATH"
	then
		break
	fi
done

case $(uname) in
	Darwin)
		HTTPD_PARA="$HTTPD_PARA -DDarwin"
	;;
esac

# Choose a port number based on the test script's number and store it in
# the given variable name, unless that variable already contains a number.
test_set_port () {
	local var=$1 port

	if test $# -ne 1 || test -z "$var"
	then
		echo >&2 "bug: test_set_port requires a variable name"
		return 1
	fi

	eval port=\$$var
	case "$port" in
	"")
		# No port is set in the given env var, use the test
		# number as port number instead.
		# Remove not only the leading 't', but all leading zeros
		# as well, so the arithmetic below won't (mis)interpret
		# a test number like '0123' as an octal value.
		port=${this_test#${this_test%%[1-9]*}}
		if test "${port:-0}" -lt 1024
		then
			# root-only port, use a larger one instead.
			port=$(($port + 10000))
		fi
		;;
	*[!0-9]*|0*)
		error >&7 "invalid port number: $port"
		;;
	*)
		# The user has specified the port.
		;;
	esac

	# Make sure that parallel '--stress' test jobs get different
	# ports.
	port=$(($port + ${GIT_TEST_STRESS_JOB_NR:-0}))
	eval $var=$port
}

check_httpd () {
	LIB_HTTPD_PATH=${LIB_HTTPD_PATH-"$DEFAULT_HTTPD_PATH"}
	test_set_port LIB_HTTPD_PORT || return

	TEST_PATH="$TEST_DIRECTORY"/lib-httpd
	HTTPD_ROOT_PATH="$PWD"/httpd
	HTTPD_DOCUMENT_ROOT_PATH=$HTTPD_ROOT_PATH/www

	# hack to suppress apache PassEnv warnings
	GIT_VALGRIND=$GIT_VALGRIND; export GIT_VALGRIND
	GIT_VALGRIND_OPTIONS=$GIT_VALGRIND_OPTIONS; export GIT_VALGRIND_OPTIONS
	GIT_TEST_SIDEBAND_ALL=$GIT_TEST_SIDEBAND_ALL; export GIT_TEST_SIDEBAND_ALL
	GIT_TRACE=$GIT_TRACE; export GIT_TRACE

	HTTPD_VERSION=$($LIB_HTTPD_PATH -v | \
			sed -n 's/^Server version: Apache\/\([0-9]*\)\..*$/\1/p; q')

	if test -n "$HTTPD_VERSION"
	then
		if test -z "$LIB_HTTPD_MODULE_PATH"
		then
			if ! test $HTTPD_VERSION -ge 2
			then
				echo >&2 "fatal: at least Apache version 2 is required"
				return 1
			fi
			if ! test -d "$DEFAULT_HTTPD_MODULE_PATH"
			then
				echo >&2 "fatal: Apache module directory not found"
				return 1
			fi

			LIB_HTTPD_MODULE_PATH="$DEFAULT_HTTPD_MODULE_PATH"
		fi
	else
		echo >&2 "fatal: Could not identify web server at '$LIB_HTTPD_PATH'"
		return 1
	fi
}

check_httpd

write_script () {
	{
		echo "#!${2-"$SHELL_PATH"}" &&
		cat
	} >"$1" &&
	chmod +x "$1"
}

install_script () {
	write_script "$HTTPD_ROOT_PATH/$1" <"$TEST_PATH/$1"
}

prepare_httpd() {
	mkdir -p "$HTTPD_DOCUMENT_ROOT_PATH"
	cp "$TEST_PATH"/passwd "$HTTPD_ROOT_PATH"
	install_script incomplete-length-upload-pack-v2-http.sh
	install_script incomplete-body-upload-pack-v2-http.sh
	install_script broken-smart-http.sh
	install_script error-smart-http.sh
	install_script error.sh
	install_script apply-one-time-perl.sh
	install_script upload.sh
	install_script list.sh

	ln -s "$LIB_HTTPD_MODULE_PATH" "$HTTPD_ROOT_PATH/modules"

	if test -n "$LIB_HTTPD_SSL"
	then
		HTTPD_PROTO=https

		RANDFILE_PATH="$HTTPD_ROOT_PATH"/.rnd openssl req \
			-config "$TEST_PATH/ssl.cnf" \
			-new -x509 -nodes \
			-out "$HTTPD_ROOT_PATH/httpd.pem" \
			-keyout "$HTTPD_ROOT_PATH/httpd.pem"
		GIT_SSL_NO_VERIFY=t
		export GIT_SSL_NO_VERIFY
		HTTPD_PARA="$HTTPD_PARA -DSSL"
	else
		HTTPD_PROTO=http
	fi
	HTTPD_DEST=127.0.0.1:$LIB_HTTPD_PORT
	HTTPD_URL=$HTTPD_PROTO://$HTTPD_DEST
	export HTTPD_URL
	HTTPD_URL_USER=$HTTPD_PROTO://user%40host@$HTTPD_DEST
	HTTPD_URL_USER_PASS=$HTTPD_PROTO://user%40host:pass%40host@$HTTPD_DEST
	GIT_EXEC_PATH=$(git --exec-path)
}

start_httpd() {
	APACHE_CONF_FILE=${1-apache.conf}

	prepare_httpd

	"$LIB_HTTPD_PATH" -d "$HTTPD_ROOT_PATH" \
		-f "$TEST_PATH/$APACHE_CONF_FILE" $HTTPD_PARA \
		-c "Listen 127.0.0.1:$LIB_HTTPD_PORT" -k start
	if test $? -ne 0
	then
		cat "$HTTPD_ROOT_PATH"/error.log
		echo >&2 "fatal: web server setup failed"
		return 1
	fi
}

stop_httpd() {
	"$LIB_HTTPD_PATH" -d "$HTTPD_ROOT_PATH" \
		-f "$TEST_PATH/$APACHE_CONF_FILE" $HTTPD_PARA -k stop
}
