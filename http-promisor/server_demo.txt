Plain http promisor remote: server side first demo
==================================================

# 1) Initial setup
##################

# Clone this repo
export LANG=C
cd /tmp
git clone git@gitlab.com:chriscool/partial-clone-demo.git

# Setup http server
TEST_DIRECTORY="/tmp/partial-clone-demo/http-promisor"
. "$TEST_DIRECTORY"/lib-httpd.sh
start_httpd apache-e-odb.conf
ps auxwww | grep apache
echo $HTTPD_URL

# Fetch initial repo used as the server repo
git clone --bare git@gitlab.com:gitlab-org/gitlab-git.git

# Configure initial repo
git -C gitlab-git.git config uploadpack.allowFilter true
git -C gitlab-git.git config uploadpack.allowAnySHA1InWant true

# Get number of missing objects in gitlab-git.git
git -C gitlab-git.git rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
0

# 2) Server configuration to use the plain HTTP promisor remote
###############################################################

# Configure an update hook on the server
cp -a "$TEST_DIRECTORY"/upload_large_files.sh /tmp/gitlab-git.git/hooks/update

# Add the HTTP promisor remote
git -C gitlab-git.git remote add httpremote "testhttpgit::${PWD}/gitlab-git.git"
git -C gitlab-git.git config remote.httpremote.promisor true

# Add path to driver for plain http promisor remote to PATH
# (It's called "git-remote-testhttpgit".)
PATH="$TEST_DIRECTORY/:$PATH"

# Source useful functions
. "$TEST_DIRECTORY"/lib-http-promisor.sh

# Upload all the large blobs to the HTTP remote
git -C gitlab-git.git rev-list --all --objects --filter-print-omitted --filter=blob:limit=800k | perl -ne 'print if s/^[~]//' >large_blobs.txt
upload_blobs_from_stdin <large_blobs.txt

# Show what blobs are now available on the HTTP server
curl "http://127.0.0.1:10000/list/"

# Remove the origin remote
git -C gitlab-git.git config --remove-section remote.origin
git -C gitlab-git.git config -l | grep origin

# Repack to prepare for cleanup
# This needs a Git where:
#   - `git repack` accepts --filter
#   - `git repack` with --filter creates a .promisor packfile
#   - `git pack-objects` accepts `--filter` without `--stdout`
#   - `git upload-pack` accepts GIT_TEST_ALLOW_MISSING_PROMISOR
#   - `git repack` accepts --missing
#   - commit fcc07e980b344 is reverted
# Like on branch 'cc-repack-filter9' available on:
# https://gitlab.com/gitlab-org/gitlab-git/
git -C gitlab-git.git -c repack.writebitmaps=false repack -a -d --filter=blob:limit=800k --missing=allow-promisor

# Get number of missing objects in gitlab-git.git
git -C gitlab-git.git rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
32

# Check a command in the new repo
git -C gitlab-git.git log

# Get one of the missing object from the plain http promisor remote
git -C gitlab-git.git show bb9c9ba37c07414e5e9ca092566500aa4a9ead87

# Get number of missing objects in gitlab-git.git
git -C gitlab-git.git rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
31

3) Client clone using the plain HTTP promisor remote
####################################################

# Clone from server repo and the plain HTTP promisor remote
GIT_TEST_ALLOW_MISSING_PROMISOR=1 git clone -c remote.httpremote.url='testhttpgit::/tmp/gitlab-git.git' -c remote.httpremote.fetch='+refs/heads/*:refs/remotes/httpremote/*' -c remote.httpremote.promisor=true --no-local --filter=blob:limit=800k gitlab-git.git git-client0

# Get number of missing objects in git-client0
git -C git-client0 rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
29

# Get number of missing objects in gitlab-git.git
git -C gitlab-git.git rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
31

4) Regular client clone and then plain HTTP promisor remote configuration
#########################################################################

# Clone from server repo into client repo
GIT_TEST_ALLOW_MISSING_PROMISOR=1 git clone --no-local --filter=blob:limit=800k gitlab-git.git git-client1

# Set a few config options
git -C git-client1 config core.repositoryformatversion 1
git -C git-client1 config extensions.partialclone "origin"

# Get number of missing objects in git-client1
git -C git-client1 rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
29

# Get number of missing objects in gitlab-git.git
git -C gitlab-git.git rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l
28
