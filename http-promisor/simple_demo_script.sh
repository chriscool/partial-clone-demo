#!/bin/sh

export LANG=C

PARTIAL_CLONE_DEMO_DIR="/tmp/partial-clone-demo"
PARTIAL_CLONE_DEMO_URL="git@gitlab.com:chriscool/partial-clone-demo.git"

GIT_REPO_AS_SERVER_REPO="git@gitlab.com:gitlab-org/gitlab-git.git"
GIT_REPO_NAME="gitlab-git.git"
GIT_MIN_SIZE="800k"
TEST_REPO_AS_SERVER_REPO="/home/christian/git/partial-test"
TEST_REPO_NAME="partial-test.git"
TEST_MIN_SIZE="5k"

# Choose Git repo by default
SERVER_REPO="$GIT_REPO_AS_SERVER_REPO"
SERVER_NAME="$GIT_REPO_NAME"
SERVER_SIZE="$GIT_MIN_SIZE"

die() {
    printf >&2 "FATAL: %s\n" "$@"
    exit 1
}

USE_TEST_REPO=0
CLEAN_REPOS=0

# Parse options
while test $# -gt 0
do
	case $1 in
		--test)
			shift # past argument
			USE_TEST_REPO=1
			;;
		--clean)
			shift # past argument
			CLEAN_REPOS=1
			;;
		*)
			die "Unknown option $1"
			;;
      esac
done

# Apply options
if test "$USE_TEST_REPO" = 1
then
	SERVER_REPO="$TEST_REPO_AS_SERVER_REPO"
	SERVER_NAME="$TEST_REPO_NAME"
	SERVER_SIZE="$TEST_MIN_SIZE"
fi

if test "$CLEAN_REPOS" = 1
then
	rm -rf /tmp/"$SERVER_NAME"
	rm -rf /tmp/git-client*
fi

GIT_VERS=$(git --version | perl -ne 'print "$1.$2" if m/git version (\d+)\.(\d+)/')
test -n "$GIT_VERS" ||
	die "Couldn't get Git version"
echo "Git version (full): $(git --version)"
echo "Git version (short): $GIT_VERS"

RECENT_GIT=$(git repack -h | grep filter-to)
test -n "$RECENT_GIT" ||
	die "Git is too old" "'git repack' should support the --filter=... and --filter-to=... options"

# 1) Initial setup
##################

printf "\nStep 1) Initial setup\n\n"

# Move to /tmp
cd /tmp

# Clone demo repo for the scripts and drivers/helpers it contains
if test -d "$PARTIAL_CLONE_DEMO_DIR"
then
	echo "Ok. Directory $PARTIAL_CLONE_DEMO_DIR exist."
else
	git clone "$PARTIAL_CLONE_DEMO_URL" ||
		die "Couldn't clone from $PARTIAL_CLONE_DEMO_URL"
fi

# Setup http server
TEST_DIRECTORY="$PARTIAL_CLONE_DEMO_DIR/http-promisor"
. "$TEST_DIRECTORY"/lib-httpd.sh
start_httpd apache-e-odb.conf
ps auxwww | grep apache | grep -v grep >ps_apache.txt

if test $(cat ps_apache.txt | wc -l) -ge 1
then
	echo "Ok. HTTP remote URL: $HTTPD_URL"
else
	die "Couldn't start apache"
fi

test -n "$HTTPD_URL" ||
	die "HTTPD_URL not set"

export HTTPD_URL

# Fetch initial repo used as the server repo
git clone --bare --no-local $SERVER_REPO ||
	die "Couldn't clone from $SERVER_REPO"

# Configure initial repo
git -C "$SERVER_NAME" config uploadpack.allowFilter true
git -C "$SERVER_NAME" config uploadpack.allowAnySHA1InWant true

# Get number of missing objects on the server repo
MISSING=$(git -C "$SERVER_NAME" rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' | wc -l)
echo "Missing objects in $SERVER_NAME: $MISSING"

# 2) Server configuration to use the plain HTTP promisor remote
###############################################################

printf "\nStep 2) Server configuration to use the plain HTTP promisor remote\n\n"

# Configure an update hook on the server
cp -a "$TEST_DIRECTORY"/upload_large_files.sh /tmp/"$SERVER_NAME"/hooks/update

# Add the HTTP promisor remote
git -C "$SERVER_NAME" remote add httpremote "testhttpgit::${PWD}/$SERVER_NAME"
git -C "$SERVER_NAME" config remote.httpremote.promisor true

# Add the location of the plain http promisor remote helper to PATH
# (It's called "git-remote-testhttpgit".)
PATH="$TEST_DIRECTORY/:$PATH"
echo "PATH set to: '$PATH'"

upload_blob() {
	HASH="$1"

	test -n "$HASH" || die "Invalid argument '$HASH'"
	HASH_SIZE=$(git -C "$SERVER_NAME" cat-file -s "$HASH") || {
		echo >&2 "Cannot get blob size of '$HASH'"
		return 1
	}

	UPLOAD_URL="$HTTPD_URL/upload/?sha1=$HASH&size=$HASH_SIZE&type=blob"

	git -C "$SERVER_NAME" cat-file blob "$HASH" >object &&
	curl --data-binary @object --include "$UPLOAD_URL"
}

upload_blobs_from_stdin() {
	while read -r blob
	do
		echo "uploading $blob"
		upload_blob "$blob" || return
	done
}

# Check if blobs are already available on the HTTP server
check_blob_uploaded() {
	curl "$HTTPD_URL/list/" >blob_list.txt ||
		die "Couldn't curl to $HTTPD_URL/list/"
	grep "500 Internal Server Error" blob_list.txt && return 1
	test $(cat blob_list.txt | wc -l) -ge 5
}


if check_blob_uploaded
then
	echo "Ok. At least 5 blobs found on $HTTPD_URL/list/"
else
	echo "Uploading blobs to the HTTP server"
	git -C "$SERVER_NAME" rev-list --all --objects --filter-print-omitted --filter=blob:limit="$SERVER_SIZE" |
		perl -ne 'print if s/^[~]//' >large_blobs.txt ||
		die "Couldn't get blobs to upload"
	upload_blobs_from_stdin <large_blobs.txt ||
		die "Couldn't upload blobs to HTTP server"
	if check_blob_uploaded
	then
		echo "Ok. At least 5 blobs found on $HTTPD_URL/list/"
	else
		die "No blob found on $HTTPD_URL/list/"
	fi
fi

# Remove the origin remote
git -C "$SERVER_NAME" config --remove-section remote.origin ||
	die "Couldn't remove 'remote.origin' section from $SERVER_NAME config"
test -z $(git -C "$SERVER_NAME" config -l | grep origin) ||
	die "Section 'remote.origin' from $SERVER_NAME config wasn't removed"

# Repack to move large blobs into a separate packfile
git -C "$SERVER_NAME" -c repack.writebitmaps=false repack -a -d \
    --filter=blob:limit="$SERVER_SIZE" --filter-to=/tmp/pack ||
	die "Couldn't move large blobs to a separate packfile"

echo "Ok. Server repo repacked using a filter"

promisor_file=$(ls "$SERVER_NAME"/objects/pack/*.pack | perl -ne 'print if s/\.pack/.promisor/')
if test -n "$promisor_file"
then
	touch "$promisor_file" &&
		echo "Ok. Created promisor file: $promisor_file"
else
	die "Couldn't find any pack ($SERVER_NAME/objects/pack/*.pack)"
fi

# Get number of missing objects on the server
git -C "$SERVER_NAME" rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_server.txt
count=$(cat missing_on_server.txt | wc -l)
test "$count" -ge 3 ||
	die "Missing object count ($count) less than 3 on server $SERVER_NAME"

echo "Ok. $count objects missing on server $SERVER_NAME"

# Let's get one missing object
git -C "$SERVER_NAME" show $(head -1 missing_on_server.txt) >first_missing_blob ||
	die "Could not get first missing object"

echo "Ok. First missing object fetched in 'first_missing_blob' file"

# Get new number of missing objects on the server
git -C "$SERVER_NAME" rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_server.txt
count2=$(cat missing_on_server.txt | wc -l)
test "$count2" -eq $(expr $count - 1) ||
	die "Missing object count ($count2) should be one less than before ($count)"

echo "Ok. Now $count2 objects missing on the server"

# 3) Client clone using the plain HTTP promisor remote
######################################################

printf "\nStep 3) Client clone using the plain HTTP promisor remote\n\n"

# Configure the server so that `git upload-pack` on it doesn't fetch
# from the promisor remote
if test "$GIT_VERS" = "2.43"
then
	git -C "$SERVER_NAME" config uploadpack.allowmissingpromisor true
else
	git -C "$SERVER_NAME" config uploadpack.missingAction allow-promisor
fi

# Clone from server repo and the plain HTTP promisor remote
GIT_TRACE=/tmp/clone_trace.log git clone \
	 -c remote.httpremote.url="testhttpgit::/tmp/$SERVER_NAME" \
	 -c remote.httpremote.fetch='+refs/heads/*:refs/remotes/httpremote/*' \
	 -c remote.httpremote.promisor=true \
	 --no-local --filter=blob:limit="$SERVER_SIZE" "$SERVER_NAME" git-client0 ||
	die "Couldn't clone to create git-client0"

echo "Ok. git-client0 cloned from $SERVER_NAME"

# Get number of missing objects in git-client0
git -C git-client0 rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_client0.txt
count=$(cat missing_on_client0.txt | wc -l)
test "$count" -ge 3 ||
	die "Missing object count ($count) less than 3 on git-client0"

echo "Ok. $count missing objects on git-client0"

# Get number of missing objects on the server
git -C "$SERVER_NAME" rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_server.txt
count=$(cat missing_on_server.txt | wc -l)
test "$count" -ge 3 ||
	die "Missing object count ($count) less than 3 on server $SERVER_NAME"

echo "Ok. $count missing objects on server $SERVER_NAME"

# 4) Regular client clone and then plain HTTP promisor remote configuration
###########################################################################

printf "\nStep 4) Regular client clone and then plain HTTP promisor remote configuration\n\n"

# Clone from server repo into another client repo
GIT_TRACE=/tmp/clone_trace.log git clone \
	 --no-local --filter=blob:limit="$SERVER_SIZE" "$SERVER_NAME" git-client1 ||
	die "Couldn't clone to create git-client1"

echo "Ok. git-client1 cloned from $SERVER_NAME"

# Set a few config options
git -C git-client1 config core.repositoryformatversion 1
git -C git-client1 config extensions.partialclone "origin"

# Get number of missing objects in git-client1
git -C git-client1 rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_client1.txt
count=$(cat missing_on_client1.txt | wc -l)
test "$count" -ge 3 ||
	die "Missing object count ($count) less than 3 on git-client1"

echo "Ok. $count missing objects on git-client1"

# Get number of missing objects on the server
git -C "$SERVER_NAME" rev-list --objects --all --missing=print | perl -ne 'print if s/^[?]//' >missing_on_server.txt
count=$(cat missing_on_server.txt | wc -l)
test "$count" -ge 3 ||
	die "Missing object count ($count) less than 3 on server $SERVER_NAME"

echo "Ok. $count missing objects on server $SERVER_NAME"
