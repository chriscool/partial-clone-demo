#!/bin/bash

# Hook script for a Git repo to upload file larger than a limit to an
# http promisor remote.

# --- Safety check, should not be run from command line
if [ -z "$GIT_DIR" ]; then
        echo "Don't run this script from the command line." >&2
        echo " (if you want, you could supply GIT_DIR then run" >&2
        echo "  $0 <ref> <oldrev> <newrev>)" >&2
        exit 1
fi

# File size limit is meant to be configured through 'hooks.filesizelimit' setting
filesizelimit=$(git config hooks.filesizelimit)

# If we haven't configured a file size limit, use default value of about 800K
if [ -z "$filesizelimit" ]; then
        filesizelimit=800000
fi

# Reference to incoming checkin can be found at $3
refname="$3"

# Find information about the file coming in that has biggest size
# We also normalize the line for excess whitespace
biggest_checkin_normalized=$(git ls-tree --full-tree -r -l "$refname" | sort -k 4 -n -r | head -1 | sed 's/^ *//;s/ *$//;s/\s\{1,\}/ /g' )

# Based on that, we can find what we are interested about
filesize=`echo $biggest_checkin_normalized | cut -d ' ' -f4,4`

# Actual comparison
if [ $filesize -gt $filesizelimit ]; then

        filename=`echo $biggest_checkin_normalized | cut -d ' ' -f5,5`

        echo "Large push attempted with file named $filename of size $filesize." >&2
        echo "Uploading the blob to an http promisor remote." >&2

	hash=`echo $biggest_checkin_normalized | cut -d ' ' -f3,3`
	UPLOAD_URL="http://127.0.0.1:10000/upload/?sha1=$hash&size=$filesize&type=blob"

	git cat-file blob "$hash" >/tmp/hook_upload_object &&
	curl --data-binary @/tmp/hook_upload_object --include "$UPLOAD_URL" >&2

	exit
fi

exit 0
